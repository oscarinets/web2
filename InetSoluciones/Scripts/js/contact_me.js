$(document).ready(function () {
    $('.trim-field').focusout(function () {
        $(this).val($(this).val().trim())
    });

    $('#contactForm input, #contactForm textarea').jqBootstrapValidation({
        preventSubmit: true,
        submitError: function ($form, event, errors) {
            $('#success').empty();
            $('#success').append('<div></div>');
            var msg = 'Campos con informaci&oacute;n inv&aacute;lida. Por favor verifique.';
            $('#success > div')
                .toggleClass('alert alert-danger')
                .html(msg)
                .delay(2500)
        },
        submitSuccess: function ($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $form.find("#name").val();
            var email = $form.find("#email").val();
            var phone = $form.find("#phone").val();
            var message = $form.find("#compose-textarea").val();

            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message

            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }

            $.ajax({
                // edit to add steve's suggestion.
                url: "/Home/SendMail",
                type: "POST",
                data:
                    JSON.stringify(
                        {
                            name: name,
                            phone: phone,
                            email: email,
                            message: message
                        }
                    ),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $('#btnSubmit').prop('disabled', true);
                },
                success: function (data) {
                    console.dir(data);
                    
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
                    $('#success > .alert-success')
                        .append(data.Mensaje);
                    $('#success > .alert-success')
                        .append('</div>');

                    if (data.Estatus === 1) {
                        // Success message
                        $('#contactForm').trigger("reset");
                    }
                },
                error: function (j, e, f) {
                    console.error(e);
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>")
                        .append("");
                    $('#success > .alert-danger').append("Lo siento " + firstName + ", parece que mi servidor de correo no est&aacute; respondiendo. <br>&#161;Por favor, int&eacute;ntelo de nuevo m&aacute;s tarde&#33;");
                    $('#success > .alert-danger').append('</div>');
                },
                complete: function () {
                    setTimeout(function () {
                        $('#btnSubmit').prop('disabled', false);
                    }, 1000);
                },
            })
        },
        filter: function () {
            return $(this).is(":visible");
        },
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name, #email, #phone, #compose-textarea').focusin(function () {
    setTimeout(function () {
        $('#success').empty();
    }, 1000);
});
