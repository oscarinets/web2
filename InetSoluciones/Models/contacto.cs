﻿namespace Inets.Web.Models
{
    public class Contacto
    {
        #region Public Properties
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        #endregion
    }
}