﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using InetSoluciones.Helpers;

namespace InetSoluciones.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendMail(string name, string phone, string email, string message)
        {

            ResponseWrapper responseWrapper = new ResponseWrapper{
                    Estatus = 0,
                    Mensaje = "Faltan datos. Intente nuevamente"
                };

            if (String.IsNullOrEmpty(name))
            {
                responseWrapper.AnadirError("Nombre es requerido");
            }

            if( String.IsNullOrEmpty(email))
            {
                responseWrapper.AnadirError("Correo electrónico es requerido");
            }

            if ( String.IsNullOrEmpty(message))
            {
                responseWrapper.AnadirError("Mensaje es requerido");
            }

            if(responseWrapper.Errores.Count > 0)
            {
                return Json(responseWrapper);
            }
            else
            {
                responseWrapper = null;
            }

            try
            {
                // Modo de mensaje: Texto plano
                string newLine = "\r\n";
                string plainMessage = "Nuevo Mensaje de contacto"+ newLine +
                    "De: " + name + "("+ email +") " + newLine +
                    "Tel.: "+ phone + newLine +
                    "Mensaje: " + message + newLine
                    ;

                // Preparar los datos para la vista 
                ViewBag.sNombre     = name;
                ViewBag.sCorreo     = email;
                ViewBag.sTelefono   = phone;
                ViewBag.sMensaje    = message.Replace("\n", "<br>");
                // Renderizar la plantilla en HTML
                ViewBag.sBase64Logo = this.FileToBase64("~/Content/img/LTSF_2.png");

                string htmlMessage = RenderViewToString(ControllerContext, "ContentMail", null);

                /**
                 * Configuracion del servicio SMTP
                 * host: mail.inets.com.mx
                 * puerto: 587
                 */
                Libraries.EmailLibrary emailLib = new Libraries.EmailLibrary("mail.inets.com.mx", 587, "contacto@inets.com.mx", "Freedpk2", false);
                //Libraries.EmailLibrary emailLib = new Libraries.EmailLibrary("localhost");
                emailLib.Send(
                    "contacto@inets.com.mx",
                    "Contacto I.Net Soluciones",
                    new List<string>{ "contacto@inets.com.mx" },
                    "Nuevo mensaje de contacto",
                    plainMessage,
                    htmlMessage
                );

                ResponseWrapper d = new ResponseWrapper
                {
                    Estatus = 1,
                    Mensaje = "¡Gracias por sus comentarios! Nos pondremos en contacto a la brevedad posible para atenderle."
                };
                d.AnadirError(message.Replace("\n", "<br>"));
                return Json(d);
            }
            catch (Exception ex)
            {
                ResponseWrapper response = new ResponseWrapper { Estatus = 0, Mensaje = "Ocurrió un error al procesar su solicitud. Intente nuevamente." };
                response.AnadirError(ex.Message);

                return Json(response);
            }
        }
        
        
        private static String RenderViewToString(ControllerContext context, String viewPath, object model = null)
        {
            context.Controller.ViewData.Model = model;
            using (var sw = new System.IO.StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(context, viewPath, null);
                var viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        public String FileToBase64(string filePath)
        {
            var fileName = Server.MapPath(filePath);

            string mimeType = "application/unknown";

            string ext = System.IO.Path.GetExtension(fileName).ToLower();

            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);

            if (regKey != null && regKey.GetValue("Content Type") != null)
            {
                mimeType = regKey.GetValue("Content Type").ToString();
            }

            Byte[] bytes = System.IO.File.ReadAllBytes(fileName);
            String stringBase64 = Convert.ToBase64String(bytes);

            var s = "data:" + mimeType + ";base64," + stringBase64;

            return s;
        }
    }
}
